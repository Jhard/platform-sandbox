using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    private enum MovementDirection { Left, Right, Down, Up, None }

    [SerializeField]
    private Weapon weapon;
    [SerializeField]
    private float groundMovementSpeed = 0;
    [SerializeField]
    private float arialMovementSpeed = 0;
    [SerializeField]
    private float maxSpeed = 0;
    [SerializeField]
    private float jumpForce = 0;
    [SerializeField]
    private float groundDrag = 0;
    [SerializeField]
    private float arialDrag = 0;

    private Animator animator;
    private Rigidbody2D rb2d;
    private Transform focusPoint;
    private NewControls controls;
    private MovementDirection horizontalMovement;
    private MovementDirection verticalMovement;
    private MovementDirection faceDirection;
    private bool isOnGround = true;
    private bool isJumping = false;

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        focusPoint = transform.GetChild(0);

        if (!focusPoint)
        {
            focusPoint = transform;
        }

        controls = new NewControls();
        controls.Player.Movement.performed += HandleMovement;
        controls.Player.Movement.canceled += HandleMovement;

        horizontalMovement = MovementDirection.None;
        verticalMovement = MovementDirection.None;
        faceDirection = MovementDirection.Right;
}

    private void OnEnable()
    {
        controls.Player.Enable();
    }

    private void OnDisable()
    {
        controls.Player.Disable();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {
        rb2d.velocity = new Vector2(Mathf.Clamp(rb2d.velocity.x, -maxSpeed, maxSpeed), rb2d.velocity.y);
        weapon.transform.position = focusPoint.position;
    }

    private void FixedUpdate()
    {
        float movementSpeed = 0;

        if (isOnGround)
        {
            movementSpeed = groundMovementSpeed;
        }
        else
        {
            movementSpeed = arialMovementSpeed;
        }

        if (horizontalMovement != MovementDirection.None && isOnGround)
        {
            animator.SetBool("IsRunning", true);
        }
        else
        {
            animator.SetBool("IsRunning", false);
        }

        if (horizontalMovement == MovementDirection.Right)
        {
            rb2d.AddForce(new Vector2(movementSpeed * Time.deltaTime, 0), ForceMode2D.Impulse);
        }
        else if (horizontalMovement == MovementDirection.Left)
        {
            rb2d.AddForce(new Vector2(-movementSpeed * Time.deltaTime, 0), ForceMode2D.Impulse);
        }

        if (verticalMovement == MovementDirection.Up && isOnGround && isJumping)
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, jumpForce);
            isJumping = false;
        }

        if (isOnGround)
        {
            rb2d.drag = groundDrag;
        }
        else
        {
            rb2d.drag = arialDrag;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ground")
        {
            isOnGround = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Ground")
        {
            isOnGround = false;
        }
    }

    private void HandleMovement(InputAction.CallbackContext context)
    {
        Vector2 inputVector = context.ReadValue<Vector2>();
        if (inputVector.x > 0.1f)
        {
            if (faceDirection == MovementDirection.Left)
            {
                rb2d.transform.Rotate(0f, 180f, 0f);
            }
            horizontalMovement = MovementDirection.Right;
            faceDirection = MovementDirection.Right;
        }
        else if (inputVector.x < -0.1f)
        {
            if (faceDirection == MovementDirection.Right)
            {
                rb2d.transform.Rotate(0f, 180f, 0f);
            }
            horizontalMovement = MovementDirection.Left;
            faceDirection = MovementDirection.Left;
        }
        else
        {
            horizontalMovement = MovementDirection.None;
        }

        if (inputVector.y > 0.1f && isOnGround)
        {
            verticalMovement = MovementDirection.Up;
            isJumping = true;
        }
        else if (inputVector.y < -0.1f)
        {
            verticalMovement = MovementDirection.Down;
        }
        else
        {
            verticalMovement = MovementDirection.None;
        }
    }

}
