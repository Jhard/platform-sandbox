using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Weapon : MonoBehaviour
{
    [SerializeField]
    private Camera mainCamera = null;

    private NewControls controls;
    private Vector2 mousePos;

    private void Awake()
    {
        controls = new NewControls();
        controls.Player.Combat.performed += HandleCombat;
    }

    private void OnEnable()
    {
        controls.Player.Enable();
    }

    private void OnDisable()
    {
        controls.Player.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 screenVector = mainCamera.WorldToScreenPoint(transform.position);
        Vector2 targetVector = mousePos - new Vector2(screenVector.x, screenVector.y);

        float angle = 180 / Mathf.PI * Mathf.Atan2(targetVector.y, targetVector.x) - 90;
        Debug.Log(angle);
        transform.rotation = Quaternion.AngleAxis(angle, new Vector3(0, 0, 1));
    }

    private void HandleCombat(InputAction.CallbackContext context)
    {
        mousePos = context.ReadValue<Vector2>();
    }
}
